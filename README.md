# GLINET VPN manager

Change VPN settings on your router from the command line. It change your VPN
location without needing to log into the web interface. Currently the program
saves an API token to disk to prevent needing to type the router password every
time the command runs. The token appears to expire after 24 hours which great
for normal usage but not ideal for setting an automated cron job. A future
iteration will allow saving the password in the config preventing the need for
any user interaction, at a cost to the security of your router password.

## Usage

Each time you run the program it will select a new random Wireguard VPN config
from your routers saved profiles. You will need to add these profiles to the
router yourself using the standard Web UI. An easy way to bulk add profiles is
to add a provider such as Mullvad to your router which will automatically add
all of the providers VPN endpoints.

**Filters**: when importing VPN configs from a provider your router will follow
a naming scheme of <userdefinedname>_servername. It is common for VPN providers
to name their servers after the country and then a number. The way filters work
is by looking at only the letters after the `_` and then searching for your
filter in the names. for example if your run `glivpn -c de` then the filter
would find all the german servers if they are named by two letter country code.
This filter is simple and will match any servername containing the searched
string, so you are welcome to name and filter your profiles anyway you like so
long as the searched string is after the last`_` in the name.

examples of names that would match the search string above are:
- proton_de5
- pia_de2
- mullvad_de123
- myawsserver_de1

### Install linux

1. [download](https://gitlab.com/severedsec/glivpn/-/packages/8209743) or build
   the binary for your system.
1. make the binary executable `chmod +x glivpn` 
1. move the binary to a recognized PATH `sudo mv glivpn_<os>_<arch>
   /usr/bin/glivpn` Change the <os> and <arch> to whatever version you
   downloaded.
1. run the program `glivpn`
1. the first time you run the program it will need you to enter the ip address
   and password for your router. Once entered it will get an api token for the
   router and save this api token and the ip address to a config file at
   ~/.glivpn

### Create a cron job 

These instructions will allow you to run the glivpn program on a cron job so
that you can for example change your VPN every X hours. This setup requires you
to use the -p=true option which will save your password in plaintext to the
config file. This is only recommended for running the script directly on your
glinet router.

1. download or build the package for the system running glivpn. The linux arm
   build should work for running locally on your router.
1. copy the binary to your router if thats where it should run. this requires
   ssh to be enabled on the glinet router. `scp ~/Downloads/glivpn_linux_arm
   root@192.168.8.1:~/`
1. ssh into the router `ssh root@192.168.8.1`
1. run the glivpn script once to set it up and save the credentials. `./glivpn
   -p=true` and then answer the setup instructions.
1. create a cron job to run the script on a schedule. you may need to google the
   format for timing cron jobs. run `crontab -e` to edit your cron jobs and add
   the following line (use `i` to begin insert). `* * * * 1/* cd ~ &&
   ./glivpn_linux_arm -c=us -p=true` and then save the config (esc key will stop
   insert and then `:wq` will save and exit).
1. begin the cron service. This is not running by default on openwrt. `crond -L
   /var/log/crond.log`
   
The above example creates a cron job that will change the vpn daily and select
vpns from the available US profiles. you can change or remove the country code
from that command.

Ensure that your `~/.glivpn` config file contains your router password otherwise you will have errors. 
