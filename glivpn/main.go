package main

import (
	"bufio"
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"
	"math/rand"
	"time"
	"flag"
)


var config_filepath string

type WireguardConfig struct {
	Address string
	Allowed_ips string
	Dns string
	End_point string
	Listen_port string
	Mtu string
	Name string
	Persistent_keepalive string
	Preshared_key string
	Private_key string
	Public_key string
}

type WireguardClientListResponse struct {
	Code float64
	Current_server string
	Peers []WireguardConfig
}


type Glivpn struct {
	ApiToken 	 string
	IpAddr	 	 string
	Password 	 string
	SavePassword bool
}


// Login to Local network GLINET router
func (g *Glivpn) Login() {

	scanner := bufio.NewReader(os.Stdin)
	var password string
	// we only need the password if there isn't one previously saved.
	if g.Password == "" {
		fmt.Println("Password for Router:")
		password, _ = scanner.ReadString('\n')
	} else {
		password = g.Password
	}

	// if the command was run to save the password then we will write it to the
	// glivpn object for later writting to config.

	if g.SavePassword && g.Password == "" {
		g.Password = strings.Trim(password, "\n")
	}

	api_url := fmt.Sprintf("https://%s/api/router/login", g.IpAddr)

	login_data := url.Values{
		"pwd": {strings.Trim(password, "\n")},
	}

	resp, err := http.PostForm(api_url, login_data)

    if err != nil {
		log.Fatal(err)
	}

	var res map[string]string

	json.NewDecoder(resp.Body).Decode(&res)
	g.ApiToken = res["token"]

	// every time we login (get a new api token) we want to write the config.
	g.WriteConfig()
}


// get list of vpn profiles
func (g *Glivpn) GetVpnServers() WireguardClientListResponse {
	api_url := fmt.Sprintf("http://%s/api/wireguard/client/list", g.IpAddr)
	req, _ := http.NewRequest("GET", api_url, nil)
	req.Header.Set("Authorization", g.ApiToken)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	var res WireguardClientListResponse
	json.NewDecoder(resp.Body).Decode(&res)

	// handle authentication failure
	if res.Code == -1 {
		log.Println("API Token invalid, Please login again")
		g.Login()
		// this has the possibility to create an infinite loop if somehow the
		// api token becomes invalid after login and before we query the vpn
		// server list. User would have to ctrl+c to exit.
		res = g.GetVpnServers()
	}

	if len(res.Peers) == 0 {
		log.Fatal("No wireguard profiles configured on router.")
	}
	return res
}


// filter primary list based on user options (only DE, IT, US, etc...)
func (g *Glivpn) SelectRandomServer(
		serverlist WireguardClientListResponse, filter string) string {

	var servers []WireguardConfig

	// filters the server list to only those containing the filter string
	if filter != "" {
		for _, v := range serverlist.Peers {
			// glinet providers use a format of profilename_servername if you
			// are creating your own profiles you must follow this pattern.
			nameparts := strings.Split(v.Name, "_")
			servername := nameparts[len(nameparts)-1]
			if strings.Contains(servername, filter) {
				servers = append(servers, v)
			}
		}
		if len(servers) == 0 {
			log.Fatal("There are no servers that match your filter")
		}
	} else {
		// if no filter is provided then all server are options
		servers = serverlist.Peers
	}

	var server string

	// handle only one available server
	if len(servers) == 1 {
		if servers[0].Name == serverlist.Current_server {
			log.Fatal("connected server is only server matching filter")
		} else {
			// no need to randomly select from one server
			server = servers[0].Name
			return server
		}
	}


	// rand requires a seed in go. seeding with time is safe enough for usecase
	rand.Seed(time.Now().UnixNano())
	// while true loop to continue trying servers until we get a server that
	// isn't our current one.
	for {
		randomIndex := rand.Intn(len(servers))
		if servers[randomIndex].Name != serverlist.Current_server {
			server = servers[randomIndex].Name
			break
		}
	}

	return server
}


// select and set random vpn from provided list
func (g *Glivpn) SetVpnServer(server_name string) {
	// format post data
	//type SetVpnServerMsg struct {
	//	Name 	string `json:"name"`
	//	Restart string   `json:"restart"`
	//}
	//msg := SetVpnServerMsg{server_name, "true"}
	//json_data , _ := json.Marshal(msg)

	url_data := url.Values{
		"name": {server_name},
		"restart": {"true"},
	}

	// create request
	api_url := fmt.Sprintf("https://%s/api/wireguard/client/start", g.IpAddr)
	req, _ := http.NewRequest("POST", api_url, bytes.NewBuffer([]byte(url_data.Encode())))
	req.Header.Set("Authorization", g.ApiToken)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	var res map[string]string

	json.NewDecoder(resp.Body).Decode(&res)

	log.Println("set vpn server to: ", server_name)
}


// Read configuration file into config struct if no configuration exists then
// generate a new one and call WriteConfig to write it.
func (g *Glivpn) ReadConfig() {

	file, err := os.Open(config_filepath)

	if err != nil {
		if err == os.ErrNotExist {

		}
		log.Fatal("can't open config file: ", err)
	}

	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&g)
	if err != nil {
		log.Fatal("can't decode config JSON: ", err)
	}
}


// create a new configuration
func (g *Glivpn) CreateConfig() {

	fmt.Println("It appears this is your first time running this program.")
	fmt.Println("You will need to provide login info for the router and the IP address.")

	scanner := bufio.NewReader(os.Stdin)

	fmt.Println("IP Address for Router (example, 192.168.8.1):")
	ip_addr, _ := scanner.ReadString('\n')
	ip_addr = strings.Trim(ip_addr, "\n")
	// test that address is valid
	resp, err := http.Get(
		fmt.Sprintf("https://%s/api/router/login", ip_addr))
	if err != nil {
		log.Fatal("unable to connect to router: ", err)
	}
	var res map[string]string

	json.NewDecoder(resp.Body).Decode(&res)
    // if the service is a GL.inet router, we expect a missing password error
	if res["msg"] != "password missing" {
		log.Fatal("IP does not appear to be a supported router")
	}
	g.IpAddr = ip_addr
	g.Login()
}


// Write current config to file
func (g *Glivpn) WriteConfig() {
	config, encode_err := json.Marshal(g)
	if encode_err != nil {
		log.Fatal("unable to write config: ", encode_err)
	}
	write_err := ioutil.WriteFile(config_filepath, config, 0660)
	if write_err != nil {
		log.Fatal("unable to write config: ", write_err)
	}
}


func main() {

	var countryFlag = flag.String("c", "", "Use only VPNs in country. Using 2 letter country code.")
	var savePass = flag.Bool("p", false, "Save the router password to config for future use. this is only recommended when running the script directly from the router")
	flag.Parse()

	// disable HTTPS certificate verification.
	// This could be improved by pinning the cert in .glivpn so we have TOFU
	http.DefaultTransport.(
		*http.Transport).TLSClientConfig = &tls.Config{
			InsecureSkipVerify: true}
	glivpn := Glivpn{SavePassword: *savePass}

	// try to load config, Create new config if one doesn't exist
	home_dir, _ := os.UserHomeDir()
	config_filepath = fmt.Sprintf("%s/.glivpn", home_dir)
	_, file_err := os.Stat(config_filepath)
	if os.IsNotExist(file_err) {
		glivpn.CreateConfig()
	} else {
		glivpn.ReadConfig()
	}

	servers := glivpn.GetVpnServers()
	new_server := glivpn.SelectRandomServer(servers, *countryFlag)
	glivpn.SetVpnServer(new_server)
}
